package com.devcamp.rest_api.BookAuthorAPI;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class rest_apiController {
    @GetMapping("/books")
    public ArrayList<Book> name(){
        ArrayList<Book> books = new ArrayList<Book>();
        Author author1 = new Author("Trần văn Ninh", "ninhtv@devcamp.edu.vn", 'm');
        Author author2 = new Author("Tôn nữ linh Giang", "giangtnl@devcamp.edu.vn", 'f');
        Author author3 = new Author("Lê nguyễn đức Thạnh", "thanhlnd@devcamp.edu.vn", 'm');
        
        Book book1 = new Book("Ngày xưa cũ", author1, 200000, 50);
        Book book2 = new Book("Ngày mới", author2, 200000, 50);
        Book book3 = new Book("Ngày tương lai", author3, 200000, 50);
        
        books.add(book1);
        books.add(book2);
        books.add(book3);
        
        return books;
    }
}
